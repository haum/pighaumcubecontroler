#! /usr/bin/env python
# -*- coding:utf8 -*-

import sacn
import threading


universe = 1
receiver = sacn.sACNreceiver()
receiver.start()
receiver.join_multicast(universe)

# receiver.register_listener('universe',callback())

@receiver.listen_on('universe', universe = universe)  # listens on universe
def callback(packet):  # packet type: sacn.DataPacket
        print(f'{packet.universe}: {packet.dmxData[:7]}')  # print the received DMX data


try:
    while True:
        pass
except KeyboardInterrupt:
    pass


receiver.stop()
for thread in threading.enumerate():
        if thread != threading.current_thread(): thread.join()
