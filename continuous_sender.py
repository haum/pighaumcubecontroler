#! /usr/bin/env python
# -*- coding:utf8 -*-
#
# continuous_sender.py
#
# Copyright © 2021 Mathieu Gaborit (matael) <mathieu@matael.org>
#
# Licensed under the "THE BEER-WARE LICENSE" (Revision 42):
# Mathieu (matael) Gaborit wrote this file. As long as you retain this notice
# you can do whatever you want with this stuff. If we meet some day, and you
# think this stuff is worth it, you can buy me a beer or coffee in return
#

"""

"""


import sacn
import time

sender = sacn.sACNsender()
sender.start()
sender.activate_output(1)
sender[1].multicast = True # keep in mind that multicast on windows is a bit different

sender.manual_flush = False # keep manual flush off as long as possible, because if it is on, the automatic

stop = False
while True:
    for i in range(1,256):
        try:
            print(f"Sending {i}")
            sender[1].dmx_data = (i,255,0,0,i)  # some test DMX data
            time.sleep(.2) # let the sender initialize itself
        except KeyboardInterrupt:
            stop = True
            break
    if stop: break

sender.stop() # stop sending out


