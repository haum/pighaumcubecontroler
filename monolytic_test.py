#! /usr/bin/env python
# -*- coding:utf8 -*-

import sacn
import threading
import time
from controller import SPI_WS2812_led

l= SPI_WS2812_led(60)

universe = 1
receiver = sacn.sACNreceiver()
receiver.start()
receiver.join_multicast(universe)
color_led =[0,10,0]

# receiver.register_listener('universe',callback())

@receiver.listen_on('universe', universe = universe)  # listens on universe
def callback(packet):  # packet type: sacn.DataPacket
        print(f'{packet.universe}: {packet.dmxData[:7]}')  # print the received DMX data
        color_led [::] = packet.dmxData[1:4]


nled =50
try:
    while True:
        for i in range(nled):
            l[i] = color_led
            l.blit()
            time.sleep(0.1)

        l[::] = [0,0,0]
        l.blit()
        time.sleep(0.5)

except KeyboardInterrupt:
    pass


receiver.stop()
for thread in threading.enumerate():
        if thread != threading.current_thread(): thread.join()


