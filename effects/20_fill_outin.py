#! /usr/bin/env python
# -*- coding:utf8 -*-
#
# 19_fill_inout.py
#
# Copyright © 2022 Mathieu Gaborit (matael) <mathieu@matael.org>
#
# Licensed under the "THE BEER-WARE LICENSE" (Revision 42):
# Mathieu (matael) Gaborit wrote this file. As long as you retain this notice
# you can do whatever you want with this stuff. If we meet some day, and you
# think this stuff is worth it, you can buy me a beer or coffee in return
#

import time

def effect(obj):
    def wrapper():
        for i in range(4):
            obj.cube[3,i,0] = obj.dimmed_colors
            obj.cube[3,0,i] = obj.dimmed_colors
            obj.cube[i,3,0] = obj.dimmed_colors
            obj.cube[0,3,i] = obj.dimmed_colors
            obj.cube[i,0,3] = obj.dimmed_colors
            obj.cube[0,i,3] = obj.dimmed_colors

        time.sleep(obj.speed)

        for i in range(2):
            # magenta
            obj.cube[2+i, i, i] = obj.dimmed_colors
            obj.cube[i, 2+i, i] = obj.dimmed_colors
            obj.cube[i, i, 2+i] = obj.dimmed_colors

            # cyan
            obj.cube[i, 2+i, 2+i] = obj.dimmed_colors
            obj.cube[2+i, i, 2+i] = obj.dimmed_colors
            obj.cube[2+i, 2+i, i] = obj.dimmed_colors

        time.sleep(obj.speed)

        for i in range(2):
            # yellow
            obj.cube[i, 2+i, 1+i] = obj.dimmed_colors
            obj.cube[i, 1+i, 2+i] = obj.dimmed_colors
            obj.cube[2+i, i, 1+i] = obj.dimmed_colors
            obj.cube[1+i, i, 2+i] = obj.dimmed_colors
            obj.cube[2+i, 1+i, i] = obj.dimmed_colors
            obj.cube[1+i, 2+i, i] = obj.dimmed_colors

        time.sleep(obj.speed)

        # first range
        for i in range(3):
            # blues
            obj.cube[1+i, i, i] = obj.dimmed_colors
            obj.cube[i, 1+i, i] = obj.dimmed_colors
            obj.cube[i, i, 1+i] = obj.dimmed_colors

            # green
            obj.cube[i, 1+i, 1+i] = obj.dimmed_colors
            obj.cube[1+i, i, 1+i] = obj.dimmed_colors
            obj.cube[1+i, 1+i, i] = obj.dimmed_colors

        time.sleep(obj.speed)

        # middle line
        for i in range(4):
            obj.cube[i,i,i] = obj.dimmed_colors

        time.sleep(obj.speed)

        # new anin requested
        if obj.new_effect:
            obj.tals.fill(blit=True)
            obj.effect_finish = True

    return wrapper


