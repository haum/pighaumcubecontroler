#! /usr/bin/env python
# -*- coding:utf8 -*-
#
# chaser.py
#
# Copyright © 2022 Mathieu Gaborit (matael) <mathieu@matael.org>
#
# Licensed under the "THE BEER-WARE LICENSE" (Revision 42):
# Mathieu (matael) Gaborit wrote this file. As long as you retain this notice
# you can do whatever you want with this stuff. If we meet some day, and you
# think this stuff is worth it, you can buy me a beer or coffee in return
#

import time
import numpy as np


def effect(obj):
    def wrapper():
        def wheel(pos):
            """Generate rainbow colors across 0-255 positions."""

            if pos < 85:
                return np.array([pos * 3, 255 - pos * 3, 0])
            elif pos < 170:
                pos -= 85
                return np.array([255 - pos * 3, 0, pos * 3])
            else:
                pos -= 170
                return np.array([0, pos * 3, 255 - pos * 3])

        for i in range(obj.tals.Ntal):
            obj.tals[i] = wheel(int(i * 256 / obj.tals.Ntal) & 255) * obj.dimmer_val
        obj.tals.blit()

        # new anin requested
        if obj.new_effect:
            obj.tals.fill(blit=True)
            obj.effect_finish = True

    return wrapper
