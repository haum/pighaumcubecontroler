#! /usr/bin/env python
# -*- coding:utf8 -*-
#
# chaser.py
#
# Copyright © 2022 Mathieu Gaborit (matael) <mathieu@matael.org>
#
# Licensed under the "THE BEER-WARE LICENSE" (Revision 42):
# Mathieu (matael) Gaborit wrote this file. As long as you retain this notice
# you can do whatever you want with this stuff. If we meet some day, and you
# think this stuff is worth it, you can buy me a beer or coffee in return
#

import time
import random

def effect(obj):
    def wrapper():
        for i in range(4):
            obj.cube[i,0,0] = obj.dimmed_colors
            obj.cube[0,i,0] = obj.dimmed_colors
            obj.cube[0,0,i] = obj.dimmed_colors
            time.sleep(obj.speed)
        for i in range(2):
            obj.cube[3,i+1,0] = obj.dimmed_colors
            obj.cube[3,0,i+1] = obj.dimmed_colors
            obj.cube[i+1,3,0] = obj.dimmed_colors
            obj.cube[0,3,i+1] = obj.dimmed_colors
            obj.cube[i+1,0,3] = obj.dimmed_colors
            obj.cube[0,i+1,3] = obj.dimmed_colors
            time.sleep(obj.speed)
        for i in range(4):
            obj.cube[i,3,3] = obj.dimmed_colors
            obj.cube[3,i,3] = obj.dimmed_colors
            obj.cube[3,3,i] = obj.dimmed_colors
            time.sleep(obj.speed)
        black = [0]*3
        for i in range(4):
            obj.cube[i,0,0] = black
            obj.cube[0,i,0] = black
            obj.cube[0,0,i] = black
            time.sleep(obj.speed)
        for i in range(2):
            obj.cube[3,i+1,0] = black
            obj.cube[3,0,i+1] = black
            obj.cube[i+1,3,0] = black
            obj.cube[0,3,i+1] = black
            obj.cube[i+1,0,3] = black
            obj.cube[0,i+1,3] = black
            time.sleep(obj.speed)
        for i in range(4):
            obj.cube[i,3,3] = black
            obj.cube[3,i,3] = black
            obj.cube[3,3,i] = black
            time.sleep(obj.speed)
    # new anin requested
        if obj.new_effect:
            obj.tals.fill(blit=True)
            obj.effect_finish = True

    return wrapper
