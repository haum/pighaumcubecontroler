#! /usr/bin/env python
# -*- coding:utf8 -*-
#
# chaser.py
#
# Copyright © 2022 Mathieu Gaborit (matael) <mathieu@matael.org>
#
# Licensed under the "THE BEER-WARE LICENSE" (Revision 42):
# Mathieu (matael) Gaborit wrote this file. As long as you retain this notice
# you can do whatever you want with this stuff. If we meet some day, and you
# think this stuff is worth it, you can buy me a beer or coffee in return
#

import time


def effect(obj):
    def wrapper():
        for i in range(obj.tals.Ntal):
            obj.tals.fill(blit=True)
            obj.tals[i] = obj.dimmed_colors
            obj.tals.blit()
            time.sleep(obj.speed)
    # new anin requested ?
        if obj.new_effect:
            obj.tals.fill(blit=True)
            obj.effect_finish = True
        else:
            for i in range(obj.tals.Ntal - 1):
                obj.tals.fill(blit=True)
                obj.tals[obj.tals.Ntal - 2 - i] = obj.dimmed_colors
                obj.tals.blit()
                time.sleep(obj.speed)
                # new anin requested ?
                if obj.new_effect:
                    obj.tals.fill(blit=True)
                    obj.effect_finish = True

    return wrapper
