#! /usr/bin/env python
# -*- coding:utf8 -*-
#
# 1_wipe.py
#
# Copyright © 2022 Mathieu Gaborit (matael) <mathieu@matael.org>
#
# Licensed under the "THE BEER-WARE LICENSE" (Revision 42):
# Mathieu (matael) Gaborit wrote this file. As long as you retain this notice
# you can do whatever you want with this stuff. If we meet some day, and you
# think this stuff is worth it, you can buy me a beer or coffee in return
#

import time


def effect(obj):
    def wrapper():
        for i in range(obj.tals.Ntal):
            obj.tals[i] = (obj.dimmed_colors)
            obj.tals.blit()
            time.sleep(obj.speed)

        for i in range(obj.tals.Ntal):
            obj.tals[i] = [0, 0, 0]
            obj.tals.blit()
            time.sleep(obj.speed)
    # new anin requested
        if obj.new_effect:
            obj.tals.fill(blit=True)
            obj.effect_finish = True

    return wrapper
