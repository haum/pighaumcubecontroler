#! /usr/bin/env python
# -*- coding:utf8 -*-
#
# chaser.py
#
# Copyright © 2022 Mathieu Gaborit (matael) <mathieu@matael.org>
#
# Licensed under the "THE BEER-WARE LICENSE" (Revision 42):
# Mathieu (matael) Gaborit wrote this file. As long as you retain this notice
# you can do whatever you want with this stuff. If we meet some day, and you
# think this stuff is worth it, you can buy me a beer or coffee in return
#

import time
import random
from itertools import product

def effect(obj):
    alltals = list(product(*[range(4)]*3))
    def wrapper():
        for i in range(10):
            for t in filter(lambda x: sum(x)==i, alltals):
                obj.cube[t] = obj.dimmed_colors
            time.sleep(obj.speed)

        # new anim requested
        if obj.new_effect:
            obj.tals.fill(blit=True)
            obj.effect_finish = True

    return wrapper
