#! /usr/bin/env python
# -*- coding:utf8 -*-
#
# chaser.py
#
# Copyright © 2022 Mathieu Gaborit (matael) <mathieu@matael.org>
#
# Licensed under the "THE BEER-WARE LICENSE" (Revision 42):
# Mathieu (matael) Gaborit wrote this file. As long as you retain this notice
# you can do whatever you want with this stuff. If we meet some day, and you
# think this stuff is worth it, you can buy me a beer or coffee in return
#

import time
import random

def effect(obj):
    def wrapper():
        l = list(range(obj.tals.Ntal))
        s = random.sample(l, 14)
        s1, s2 = s[:7], s[7:]
        for it in s1:
            obj.tals[it] = obj.dimmed_colors
        obj.tals.blit()
        time.sleep(obj.speed)
        for it in s2:
            obj.tals[it] = obj.dimmed_colors
        obj.tals.blit()
        time.sleep(obj.speed)
        for it in s1:
            obj.tals[it] = [0]*3
        obj.tals.blit()
        time.sleep(obj.speed)
        for it in s2:
            obj.tals[it] = [0]*3
        obj.tals.blit()
        time.sleep(obj.speed)
    # new anin requested
        if obj.new_effect:
            obj.tals.fill(blit=True)
            obj.effect_finish = True

    return wrapper
