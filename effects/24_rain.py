#! /usr/bin/env python
# -*- coding:utf8 -*-
#
# chaser.py
#
# Copyright © 2022 Mathieu Gaborit (matael) <mathieu@matael.org>
#
# Licensed under the "THE BEER-WARE LICENSE" (Revision 42):
# Mathieu (matael) Gaborit wrote this file. As long as you retain this notice
# you can do whatever you want with this stuff. If we meet some day, and you
# think this stuff is worth it, you can buy me a beer or coffee in return
#

import time
import random

class AutoTal:
    def __init__(self, start, talid):
        self.start = start
        self.talid = talid

    def animate(self, obj, t):
        if t >= self.start:
            lt = t - self.start
            lt = lt/7 if lt < 7 else max(0, (14-lt)/7)
            obj.cube[self.talid] = obj.dimmed_colors * lt


def effect(obj):
    def wrapper():
        lines = random.sample(list(filter(lambda _: len(_)>1, obj.cube.v)), 3)
        phases = [random.randint(0, 4*7-7*len(l))-1 for l in lines]

        autotals = [[AutoTal(phases[il]+7*i, talid) for i, talid in enumerate(l)] for il, l in enumerate(lines)]

        for t in range(35):
            for line_at in autotals:
                for at in line_at:
                    at.animate(obj, t)
            obj.tals.blit()
            time.sleep(obj.speed/40)

        # new anim requested
        if obj.new_effect:
            obj.tals.fill(blit=True)
            obj.effect_finish = True

    return wrapper
