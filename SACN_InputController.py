#! /usr/bin/env python
# -*- coding:utf8 -*-

import os
import threading
import time
import importlib

import daemon
import sys
import os
import signal

import numpy as np
import sacn

from controller import CubeController

# from chaser import chaser
# from ping_pong import ping_pong



class InputController:
    def __init__(self, universe=1, first_channel=0, max_delay=2, effects_dir="effects"):
        """ receive SACN data """

        self.universe = universe
        self.first_channel = first_channel
        self.max_delay = max_delay

        self.receiver = sacn.sACNreceiver()
        self.receiver.start()
        self.receiver.join_multicast(self.universe)

        self.cube = CubeController()
        self.tals = self.cube.tals

        self.effects_dir = effects_dir
        self.effects_map = self.__create_effects_map()
        # self.effects_map = {
        #     0: self.fill,
        #     2: chaser(self),
        #     1: self.wipe,
        #     3: ping_pong(self)
        #
        # }

        self.dxmdata = [0] * 7
        self.dimmer = 1
        self.colors = np.zeros((3,), dtype=np.uint8)
        self.preprog = 0
        self.effect = 0
        self.speed = self.max_delay
        self.fresh_dmx = False

        self.__stop_flag = False

        # used for effect management
        self.last_effect = 0
        self.new_effect = True
        self.effect_finish = True

        self.ledth = threading.Thread(target=self.led_control)
        self.ledth.start()

        self.receiver.register_listener(
            'universe',
            self.process_packet,
            # partial(self.process_packet, self=self),
            universe=self.universe
        )

    def __create_effects_map(self):
        all_effects_files = os.listdir(self.effects_dir)
        effects_map = {}
        for f in all_effects_files:
            if not f.endswith('.py'):
                continue
            idx = int(f.split('_')[0])
            modname = f.split('.')[0]
            mod = importlib.import_module(self.effects_dir + "." + modname)
            effects_map[idx] = mod.effect(self)
        effects_map[0] = self.fill
        return effects_map

    @property
    def dimmed_colors(self):
        if self.preprog == 0:
            return np.array(self.colors * self.dimmer, dtype = np.uint8)
        else:
            return np.array(self.wheel() * self.dimmer, dtype = np.uint8)

    @property
    def dimmer_val(self):
        return self.dimmer

    def process_packet(self, packet):
        self.dxmdata = packet.dmxData[self.first_channel:self.first_channel + 8]

        # channel mapping
        # 1: dimmer
        self.dimmer = self.dxmdata[0] / 255

        # 1, 2, 3: color channels
        self.colors = np.array([self.dxmdata[2], self.dxmdata[1], self.dxmdata[3]])

        # 4: color preprog
        self.preprog = self.dxmdata[4]

        # 5: effect
        num_effect = int(self.dxmdata[5] / 6)  # 21 effects possible
        if num_effect != self.last_effect:
            self.last_effect = num_effect
            self.effect = num_effect
            self.new_effect = True
            self.effect_finish = False

        # 6: speed
        dmx_speed = self.dxmdata[6] / 255.0
        #self.speed = max(0.005, 1 - dmx_speed**3) * self.max_delay
        self.speed = (0.005 - (dmx_speed - 1)**7) * self.max_delay

        """
        si channel 5 < 10
            color.r = channel 2 / dimmer
            color.v = channel 3 / dimmer
            color.b = channel 4 / dimmer
        sinon
            color[r,v,b] = f( channel 5 ) / dimmer
        """
        # self.fresh_dmx = True

    def led_control(self):
        while True:
            if self.__stop_flag:
                break

            if self.new_effect and self.effect_finish:
                effect_fun = self.effects_map.get(self.effect, None)
                self.effect_finish = False
                self.new_effect = False
            if effect_fun is not None:
                effect_fun()
            else:
                self.effect_finish = True

    # @self.register_effect(0)
    def fill(self):
        self.tals.fill(self.dimmed_colors)
        self.effect_finish = True

    # def wipe(self):
    #     for i in range(20):
    #         self.tals[i] = (self.dimmed_colors)
    #         self.tals.blit()
    #         time.sleep(self.speed)
    #
    #     for i in range(20):
    #         self.tals[i] = [0, 0, 0]
    #         self.tals.blit()
    #         time.sleep(self.speed)
    #
    #     if self.new_effect:
    #         self.effect_finish = True

    def wheel(self):
        """Generate rainbow colors across 0-255 positions."""
        pos = self.preprog
        if pos < 85:
            return np.array([pos * 3, 255 - pos * 3, 0])
        elif pos < 170:
            pos -= 85
            return np.array([255 - pos * 3, 0, pos * 3])
        else:
            pos -= 170
            return np.array([0, pos * 3, 255 - pos * 3])

    def close(self):
        self.__stop_flag = True
        self.tals.stop()
        self.receiver.stop()
        self.ledth.join()
        for thread in threading.enumerate():
            if thread != threading.current_thread():
                thread.join()

do_run = True

def shutdown(signum, frame):  # signum and frame are mandatory
    print("shutdown")
    global do_run
    do_run = False
    # sys.exit(0)


if __name__ == "__main__":
    with daemon.DaemonContext(
        chroot_directory = None,
        working_directory='/home/pi/Documents/PiGHaumCubeControler/',
        uid=1000,
        gid=1000,
        initgroups=[999],
        stderr=sys.stderr,
        stdout=sys.stdout,
        signal_map={
            signal.SIGTERM: shutdown,
            signal.SIGTSTP: shutdown
        }):
        started = False
        while not started:
            try:
                c = InputController(universe=11)
                started = True
            except OSError:
                time.sleep(5)
        while do_run:
            time.sleep(5)
        c.close()
        sys.exit(0)
