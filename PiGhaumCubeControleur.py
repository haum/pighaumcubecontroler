#! /usr/bin/env python
# -*- coding:utf8 -*-


import time

from SACN_InputController import *
from controller import SPI_WS2812_led


nled =60
l = SPI_WS2812_led(nled)
c = InputController()

try:
    while True:
        for i in range(nled):
            l[i] = c.colors
            l.blit()
            time.sleep(1/(c.speed+1))

        l[::] = [0,0,0]
        l.blit()
        time.sleep(1/(c.speed+1))

except KeyboardInterrupt:
    pass

l[::] = [0,0,0]
l.blit()
c.close()
