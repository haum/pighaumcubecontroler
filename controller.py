#! /usr/bin/env python
# -*- coding:utf8 -*-

import spidev
from ws2812 import write2812_numpy8 as write2812
import numpy as np


class SPI_WS2812_led:
    def __init__(self, Nbled=128):

        """
        Parameters
        ----------

        Nbled: int
            Number of connected ws2812_led

        """
        self.Nbled = Nbled
        self.spi = spidev.SpiDev()
        self.spi.open(0,0)
        self._leds = np.zeros((self.Nbled,3), dtype=np.uint8)

    def fill(self, color=[0, 0, 0], blit=False):
        self._leds = np.array(color).reshape(1, -1).repeat(self.Nbled, axis=0)
        write2812(self.spi, self._leds)

    def stop(self, reset=True):
        """Stop the SPI connection"""
        if reset:
            self.fill(blit=True)
        self.spi.close()

    def __getitem__(self, ledid):
        return self._leds[ledid]

    def __setitem__(self, ledid, color):
        self._leds[ledid,:] = color

    def blit(self):
        write2812(self.spi, self._leds)


class TalController:

    def __init__(self, Ntal=64, Nleds=3):
        self.Ntal = Ntal
        self.Nleds = Nleds
        self.leds = SPI_WS2812_led(Nleds*Ntal)

    def blit(self):
        self.leds.blit()

    def fill(self, color=[0, 0, 0], blit=False):
        self.leds.fill(color, blit)

    def stop(self, reset=True):
        self.leds.stop(reset)

    def __setitem__(self, k, v):
        self.leds[self.Nleds*k:self.Nleds*k+self.Nleds] = v


class CubeController:

    def __init__(self):

        self.tals = TalController()
        self.m = np.array([1,35,38,40,15,25,33,32,18,23,28,31,20,21,22,30,55,45,43,41,5,2,36,39,13,16,26,34,12,19,24,29,58,53,48,42,63,56,46,44,8,6,3,37,11,14,17,27,60,52,51,50,61,59,54,49,62,64,57,47,10,9,7,4]).reshape(4,4,4)
        self.v = self.__get_verticals()
        self.autoblit = True

    def __get_verticals(self):
        l = []
        l.append([(i,i,i) for i in range(4)])
        l.append([(1+i, i, i) for i in range(3)])
        l.append([(i, 1+i, i) for i in range(3)])
        l.append([(i, i, 1+i) for i in range(3)])
        l.append([(i, 1+i, 1+i) for i in range(3)])
        l.append([(1+i, i, 1+i) for i in range(3)])
        l.append([(1+i, 1+i, i) for i in range(3)])
        l.append([(i, 2+i, 1+i) for i in range(2)])
        l.append([(i, 1+i, 2+i) for i in range(2)])
        l.append([(2+i, i, 1+i) for i in range(2)])
        l.append([(1+i, i, 2+i) for i in range(2)])
        l.append([(2+i, 1+i, i) for i in range(2)])
        l.append([(1+i, 2+i, i) for i in range(2)])
        l.append([(2+i, i, i) for i in range(2)])
        l.append([(i, 2+i, i) for i in range(2)])
        l.append([(i, i, 2+i) for i in range(2)])
        l.append([(i, 2+i, 2+i) for i in range(2)])
        l.append([(2+i, i, 2+i) for i in range(2)])
        l.append([(2+i, 2+i, i) for i in range(2)])
        for i in range(4):
            l.append([(3,i,0)])
            l.append([(3,0,i)])
            l.append([(i,3,0)])
            l.append([(0,3,i)])
            l.append([(i,0,3)])
            l.append([(0,i,3)])
        return l

    def __setitem__(self, k, v):
        for i in self.m[k].reshape(-1,)-1:
            self.tals[i] = v
        if self.autoblit: self.blit()

    def fill(self, val=[0]*3):
        self.tals.fill(val)
        if self.autoblit: self.blit()

    def blit(self):
        self.tals.blit()

    def stop(self):
        self.tals.stop()
