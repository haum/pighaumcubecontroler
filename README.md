# PiGHaumCubeControler


Un controleur SACN-E1.31 pour le GHaumCube


note:

la lib https://github.com/joosteto/ws2812-spi sur Raspberry4/3


When using SPI the ledstring is the only device which can be connected to the SPI bus. Both digital (I2S/PCM) and analog (PWM) audio can be used.

Many distributions have a maximum SPI transfer of 4096 bytes. This can be changed in /boot/cmdline.txt by appending

spidev.bufsiz=32768

On a RPi 3 you have to change the GPU core frequency to 250 MHz, otherwise the SPI clock has the wrong frequency. Do this by adding the following line to /boot/config.txt and reboot.

core_freq=250

On a RPi 4 its dynamic frequency clocking has to be disabled, since it will desync the SPI clock. Do this by adding this line to /boot/config.txt. (core_freq does not have to be changed, since the default value of 500MHz is SPI compatible)

core_freq_min=500

SPI requires you to be in the gpio group if you wish to control your LEDs without root.
